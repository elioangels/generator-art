# -*- coding: utf-8 -*
import argparse
from art.art import art


def run(args):
    elioway_path = args.elioway_path
    art_type = args.art_type
    print("elioway_path", elioway_path)
    art(elioway_path, art_type)


def main():
    parser = argparse.ArgumentParser(
        description="Utility for generating artwork in elioWay applications and modules."
    )
    parser.add_argument(
        "elioway_path", nargs="?", help="A elioway project folder.", type=str
    )
    parser.add_argument("--art_type", help="Accepts logo|splash|icon", type=str)
    parser.set_defaults(func=run)
    args = parser.parse_args()
    args.func(args)
