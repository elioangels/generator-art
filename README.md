![](https://elioway.gitlab.io/elioangels/generator-art/elio-generator-art-logo.png)

> Steal big, **the elioWay**

# generator-art ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Artwork and resources used for branding things **the elioWay**.

-   [generator-art Documentation](https://elioway.gitlab.io/elioangels/generator-art)

# Installing

```shell
python3 -m venv venv-generator-art
# or
virtualenv --python=python3 venv-generator-art
# then
source venv-generator-art/bin/activate.fish
# or
source venv-generator-art/bin/activate
# then
pip install -r requirements/local.txt
```

-   [Installing generator-art](https://elioway.gitlab.io/elioangels/generator-art/installing.html)

# Nutshell

## Usage

```shell
python elio-art <dir>
```

-   [generator-art Quickstart](https://elioway.gitlab.io/elioangels/generator-art/quickstart.html)
-   [generator-art Credits](https://elioway.gitlab.io/elioangels/generator-art/credits.html)

![](https://elioway.gitlab.io/elioangels/generator-art/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
