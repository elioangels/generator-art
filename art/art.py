# -*- coding: utf-8 -*
import logging
import os
import shutil
from pathlib import Path
from yaml import safe_load, dump

from .drawing import (
    draw_elio,
    draw_elio_logo,
    draw_elioway_icon,
    draw_logo,
    draw_postcard,
    draw_star,
    draw_star_icon,
    ELIO72_HALO_WIDTH,
    FAVICON_ICO_SIZES,
    LOGO_HEIGHT,
    TILE_HEIGHT,
)
from .utils import yamlme


logger = logging.getLogger(__name__)

PROJECTROOT = "~/Dev"

def _get_elioway_root_path():
    try:
        path = Path(os.path.expanduser(f"{PROJECTROOT}/elioway")).resolve()
        return str(path.parent)
    except KeyError:
        error_msg = "Set the {} env variable".format(key)
        print("ImproperlyConfigured: {}".format(error_msg))
        raise ImproperlyConfigured(error_msg)


def logo(elio_path, artwork_yaml):
    logo = draw_logo(artwork_yaml)
    identifier = artwork_yaml["identifier"]
    subjectOf = artwork_yaml["subjectOf"]
    app_name = subjectOf if subjectOf else identifier
    plugin_name = identifier if subjectOf else ""
    logo_name = f"{app_name}-{plugin_name}" if plugin_name else app_name
    if logo_name.startswith("elio"):
        logo_name = logo_name[3:]
    logo.save(
        os.path.join(elio_path, f"elio-{logo_name}-logo.png"), "PNG",
    )


def splash(elio_path, app_yaml):
    splash_art = os.path.join(elio_path, "artwork", "splash.jpg")
    icon_art = os.path.join(elio_path, "artwork", "icon.png")
    if not os.path.exists(splash_art) or not os.path.exists(icon_art):
        print(" |- No splash art. Skipping.")
        return
    if os.path.exists(os.path.join(elio_path, "splash.jpg")):
        os.remove(
            os.path.join(elio_path, "splash.jpg"),
        )
    postcard = draw_postcard(splash_art, icon_art, app_yaml)
    postcard.save(os.path.join(elio_path, "postcard.jpg"))


def icons(elio_path, artwork_yaml):
    icon_art = os.path.join(elio_path, "artwork", "icon.png")
    if not os.path.exists(icon_art):
        print(" |- No icon art. Using blank.")
        shutil.copyfile("/home/tim/Dev/elioway/eliosin/icon/eliostar/artwork/icon.png",icon_art)
    # star.png
    draw_star_icon(icon_art, TILE_HEIGHT, 72, ELIO72_HALO_WIDTH,).save(
        os.path.join(elio_path, "star.png")
    )
    # draw_star_icon(icon_art, TILE_HEIGHT, 72, ELIO72_HALO_WIDTH, inverted=True,).save(
    #     os.path.join(elio_path, "staverted.png")
    # )
    # apple-touch-icon.png
    draw_star_icon(icon_art, LOGO_HEIGHT, 72, 7).save(
        os.path.join(elio_path, "apple-touch-icon.png")
    )
    # draw_star_icon(icon_art, LOGO_HEIGHT, 72, 7, inverted=True).save(
    #     os.path.join(elio_path, "apple-touch-icant.png")
    # )
    # draw_star_icon(icon_art, 48, 72, 2, inverted=True).save(
    #     os.path.join(elio_path, "butoff.png")
    # )
    # favicons: Use the specially prepared file if exists.
    favicon_file = os.path.join(elio_path, "artwork", "favicon.png")
    if os.path.exists(favicon_file):
        icon_art = favicon_file
    # favicon.ico
    draw_star_icon(icon_art, 64, 24).save(
        os.path.join(elio_path, "favicon.ico"), sizes=FAVICON_ICO_SIZES,
    )
    # favicon.png
    draw_star_icon(icon_art, 128, 24).save(
        os.path.join(elio_path, "favicon.png")
    )
    # favicoff.png
    draw_star_icon(icon_art, 128, 24, inverted=True).save(
        os.path.join(elio_path, "favicoff.png")
    )

    elio_url = artwork_yaml.get("url")
    if elio_url:
        import qrcode
        qrcode_art = os.path.join(elio_path, "artwork", "qrcode.png")
        qrcode_star = os.path.join(elio_path, "qrcode.png")
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=30,
            border=8,
        )
        qr.add_data(elio_url)
        qr.make(fit=True)
        # Transparent BG
        img = qr.make_image(fill_color="black", back_color="white")
        img = img.convert("RGBA")
        datas = img.getdata()
        newData = []
        for item in datas:
            if item[0] == 255 and item[1] == 255 and item[2] == 255:
                newData.append((255, 255, 255, 0))
            else:
                newData.append(item)
        img.putdata(newData)
        # Save full verion
        img.save(qrcode_art, "PNG")
        # Creare star version
        draw_star_icon(qrcode_art, img.size[1], 72).save(
            qrcode_star
        )



def art_for(elio_path):
    """Art for the given elioWay app."""
    yaml_file = os.path.join(elio_path, "artwork", "artwork.yaml")
    artwork_yaml = safe_load(open(yaml_file, "r"))
    icons(elio_path, artwork_yaml)
    logo(elio_path, artwork_yaml)
    splash(elio_path, artwork_yaml)


def art_everything(elioway_root, arg_path):
    """Art for all the elioway folders in this app."""
    walk_path = os.path.join(elioway_root, arg_path)
    for root, dirs, files in os.walk(walk_path):
        for dir in dirs:
            if dir == "artwork" and not "node_modules" in root:
                elio_path = os.path.join(root, dir).replace(elioway_root, "")
                artwork_path = Path(elioway_root + elio_path)
                yaml_file = os.path.join(artwork_path, "artwork.yaml")
                if os.path.exists(yaml_file):
                    elio_app_path = os.fspath(artwork_path.parent)
                    print(" > art_for", elio_app_path)
                    art_for(elio_app_path)


def art(arg_path, art_type):
    elioway_root = _get_elioway_root_path()
    print("elioway_root", elioway_root)
    print("arg_path", arg_path)
    print("art_type", art_type)
    art_everything(elioway_root, arg_path)
