# -*- coding: utf-8 -*
import fileinput
import os
import string
from yaml import load, dump


def txt_asc(txt):
    """Text contains ascender characters, eg: b, d, j or A."""
    return bool([c for c in txt if c in "bdfhjklt" + string.ascii_uppercase])


def txt_dsc(txt):
    """Text contains descender characters, eg:  p, q or j."""
    return bool([c for c in txt if c in "gjpqy"])


def yamlme(elio_path):
    yaml_file = os.path.join(elio_path, "artwork", "artwork.yaml")
    if os.path.exists(yaml_file):
        artwork_yaml = load(open(yaml_file, "r"))
    if not artwork_yaml.get("alternateName"):
        readme_file = os.path.join(elioway_root, elio_path, "README.md")
        with fileinput.input(files=(readme_file)) as f:
            for line in f:
                if ">" in line:
                    artwork_yaml["alternateName"] = line[1:].strip()
                    dump(
                        artwork_yaml,
                        open(yaml_file, "w"),
                        default_flow_style=False,
                    )
                    break
