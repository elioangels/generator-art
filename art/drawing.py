"""A set of utils which each draw a different type of artwork. None of
these should write to file."""
# -*- coding: utf-8 -*
import gizeh as gz
import numpy as np
import os
import string
import textwrap

from datetime import date
from PIL import Image, ImageDraw, ImageFont, ImageOps
from .utils import txt_asc, txt_dsc

MONTSERRAT_BOLD = os.path.abspath(__file__).replace(
    "art/drawing.py", "fonts/Montserrat-Bold.ttf"
)
MONTSERRAT_REG = os.path.abspath(__file__).replace(
    "art/drawing.py", "fonts/Montserrat-Regular.ttf"
)
MONTSERRAT_EXTRABOLD = os.path.abspath(__file__).replace(
    "art/drawing.py", "fonts/Montserrat-ExtraBold.ttf"
)
for _font_path in [MONTSERRAT_BOLD, MONTSERRAT_REG, MONTSERRAT_EXTRABOLD]:
    if not os.path.exists(_font_path):
        _font = os.path.split(_font_path)[-1]
        raise Exception(f"{_font} not found.")


TILE_HEIGHT = 558
LOGO_HEIGHT = 180
ELIO72_HALO_WIDTH = 20

WHITE = (1, 1, 1)
BLACK = (0, 0, 0)

TEXT_ASCENDANT = -20
TEXT_DESCENDANT = 15
LOGO_TEXT_HALO = 40
ELIO72_LOGO_HALO_OVERLAP = int(LOGO_TEXT_HALO / 2)
LOGO_TEXT_SIZE = 230
LOGO_PLUGIN_TEXT_SIZE = 130
TEXT_CROPPED_ISSUE_FIX = 10

FAVICON_ICO_SIZES = [
    (256, 256),
    (128, 128),
    (64, 64),
    (48, 48),
    (32, 32),
    (24, 24),
    (16, 16),
]


def _invert_rgba(image):
    r, g, b, a = image.split()
    rgb_image = Image.merge("RGB", (r, g, b))
    inverted_image = ImageOps.invert(rgb_image)
    r2, g2, b2 = inverted_image.split()
    return Image.merge("RGBA", (r2, g2, b2, a))


def draw_elio():
    surface = gz.Surface(width=TILE_HEIGHT, height=TILE_HEIGHT)
    draw_star(
        surface, [surface.height / 2, surface.height / 2], 72, 0, WHITE
    ).draw(surface)
    draw_star(
        surface,
        [surface.height / 2, surface.height / 2],
        72,
        ELIO72_HALO_WIDTH,
        BLACK,
    ).draw(surface)
    draw_elio_logo(
        [surface.height / 2, surface.height / 2 + TEXT_ASCENDANT]
    ).draw(surface)
    return Image.fromarray(surface.get_npimage(transparent=True))


def draw_elio_logo(xy, fill=WHITE):
    return gz.text(
        "elio",
        "Montserrat",
        LOGO_TEXT_SIZE,
        fontweight="bold",
        fill=fill,
        h_align="center",
        v_align="center",
        xy=xy,
    )


def draw_elioway_icon():
    surface = gz.Surface(width=TILE_HEIGHT, height=TILE_HEIGHT)
    draw_elio_logo(
        [surface.height / 2, surface.height / 2 + TEXT_ASCENDANT], BLACK
    ).draw(surface)
    return Image.fromarray(surface.get_npimage(transparent=True))


def _draw_logo(artwork_yaml):
    app_font = ImageFont.truetype(MONTSERRAT_EXTRABOLD, LOGO_TEXT_SIZE)
    plugin_font = ImageFont.truetype(MONTSERRAT_EXTRABOLD, LOGO_PLUGIN_TEXT_SIZE)
    identifier = artwork_yaml["identifier"]
    subjectOf = artwork_yaml["subjectOf"]
    app_name = subjectOf if subjectOf else identifier
    plugin_name = identifier if subjectOf else ""
    # Meaure the identifier text
    app_name_width, lh = app_font.getsize(app_name)
    plugin_name_width, ph = plugin_font.getsize(plugin_name)
    ascendant = TEXT_ASCENDANT if txt_asc(app_name) else 0
    descendant = TEXT_DESCENDANT if txt_dsc(app_name) else 0
    # Create a surface
    surface = gz.Surface(
        width=TILE_HEIGHT
        + app_name_width
        + plugin_name_width
        + ELIO72_LOGO_HALO_OVERLAP + TEXT_CROPPED_ISSUE_FIX,
        height=TILE_HEIGHT,
    )

    for stroke_width in [LOGO_TEXT_HALO, 0]:
        gz.text(
            app_name,
            "Montserrat",
            LOGO_TEXT_SIZE,
            fontweight="bold",
            fill=BLACK,
            h_align="left",
            stroke=(0.8, 0.8, 0.8),
            stroke_width=stroke_width,
            v_align="center",
            xy=[
                TILE_HEIGHT + plugin_name_width - ELIO72_LOGO_HALO_OVERLAP,
                surface.height / 2 + ascendant + descendant + 5,
            ],
        ).draw(surface)
        gz.text(
            plugin_name,
            "Montserrat",
            LOGO_PLUGIN_TEXT_SIZE,
            fontweight="bold",
            fill=BLACK,
            h_align="left",
            stroke=(1,1,1),
            stroke_width=stroke_width,
            v_align="center",
            xy=[
                ELIO72_LOGO_HALO_OVERLAP,
                surface.height / 3 + ascendant + descendant,
            ],
        ).draw(surface)
    # Draw the elio star
    draw_star(
        surface,
        [surface.height / 2 + plugin_name_width, surface.height / 2],
        72,
        0,
        WHITE,
    ).draw(surface)
    draw_star(
        surface,
        [surface.height / 2 + plugin_name_width, surface.height / 2],
        72,
        ELIO72_HALO_WIDTH,
        BLACK,
    ).draw(surface)
    draw_elio_logo(
        [
            surface.height / 2 + plugin_name_width,
            surface.height / 2 + TEXT_ASCENDANT,
        ],
    ).draw(surface)
    return Image.fromarray(surface.get_npimage(transparent=True))


def draw_logo(artwork_yaml):
    # Prep for resize and export
    logo = _draw_logo(artwork_yaml)
    return logo.resize(
        (int(logo.width * (LOGO_HEIGHT / TILE_HEIGHT)), LOGO_HEIGHT),
        resample=Image.BICUBIC,
    )


def draw_postcard(splash_art, icon_art, app_yaml):
    bigger = 2
    # ================================
    # Canvas
    postcard = Image.new(
        size=(
            529 * bigger,
            336 * bigger,
        ),  # (splash.width, splash.height + app_logo.height + 20),
        mode="RGBA",
        color="WHITE",
    )
    # ================================
    # Splash
    splash = Image.open(splash_art).convert(mode="RGBA")
    splash = splash.resize(
        (postcard.width, int(postcard.width / splash.width * splash.height)),
        resample=Image.BICUBIC,
    )
    postcard.paste(splash, (0, 0), splash)
    strip_height = int(postcard.height - splash.height)
    # Logo
    logo_height = int(strip_height / 1.2)
    app_logo = _draw_logo(app_yaml)
    app_logo = app_logo.resize(
        (int(logo_height / app_logo.height * app_logo.width), logo_height),
        resample=Image.BICUBIC,
    )
    postcard.paste(
        app_logo, (2, postcard.height - app_logo.height), app_logo,
    )
    # ================================
    # Star
    star = draw_star_icon(icon_art, TILE_HEIGHT, 72, ELIO72_HALO_WIDTH,)
    star = star.resize(
        (int(logo_height / star.height * star.width), logo_height),
        resample=Image.BICUBIC,
    )
    postcard.paste(
        star,
        (postcard.width - star.width, postcard.height - star.height),
        star,
    )
    # ================================
    # Quote
    # quote_indent = int(postcard.width / 50)
    # quote_font_size = 15 * bigger  # int(postcard.width / 40) * 20
    # quote_max_width = postcard.width - (quote_indent * 6)  # star.width - 20
    # quote_text = app_yaml.get("alternateName") # + ","
    # if quote_text.endswith("."):
    #     quote_text = quote_text[0:-1]
    # if not "the elioWay" in quote_text:
    #     quote_text += ", the elioWay"
    # f = ImageFont.truetype(MONTSERRAT_BOLD, quote_font_size)
    # quotes = quote_text.split(" ")
    # Ensure we get the last word.
    #
    # quotes.append(" " * 1000)
    # current_line = ""
    # x = 0
    # for word in quotes:
    #     # Look ahead at width of current_line
    #     lookahead_w, lookahead_h = f.getsize(current_line + word)
    #     if lookahead_w > quote_max_width:
    #         w, h = f.getsize(current_line.strip())
    #         surface = gz.Surface(width=w + TEXT_CROPPED_ISSUE_FIX, height=h)
    #         t = gz.text(
    #             current_line.strip(),
    #             "Montserrat",
    #             quote_font_size,
    #             fontweight="bold",
    #             fill=BLACK,
    #             h_align="left",
    #             v_align="bottom",
    #             xy=[0, 0],
    #         ).draw(surface)
    #         quote = Image.fromarray(surface.get_npimage(transparent=True))
    #         postcard.paste(
    #             quote,
    #             (
    #                 int(postcard.width / 2) - int(quote.width / 2),
    #                 (x * h) + splash.height + quote_indent,
    #             ),
    #             quote,
    #         )
    #         # Reset current_line to the current word
    #         current_line = word + " "
    #         x += 1
    #     else:
    #         current_line += word + " "
    # URL
    # url = "https://elioway.gitlab.io"
    # url_font_size = 8 * bigger
    # f = ImageFont.truetype(MONTSERRAT_REG, url_font_size)
    # w, h = f.getsize(url)
    # surface = gz.Surface(width=w + 5, height=h + 5)
    # uw, uh = f.getsize(url)
    # t = gz.text(
    #     url,
    #     "Montserrat",
    #     url_font_size,
    #     fill=BLACK,
    #     h_align="left",
    #     v_align="bottom",
    #     xy=[0, 0],
    # ).draw(surface)
    # url_elioway = Image.fromarray(surface.get_npimage(transparent=True))
    # postcard.paste(
    #     url_elioway,
    #     (
    #         app_logo.width + quote_indent,
    #         postcard.height - url_elioway.height - 2,
    #     ),
    #     url_elioway,
    # )
    return postcard.convert(mode="RGB")


def draw_star(surface, xy, points, halo, fill):
    ratio = (96.14 - np.pi * (72 / points)) / 100
    return gz.star(
        fill=fill,
        nbranches=points,
        ratio=ratio,
        radius=(surface.height - (halo + halo)) / 2,
        xy=xy,
    )


def draw_star_icon(icon_art, size, points, halo=0, inverted=False):
    # Start with a square surface and use gizah to draw the star.
    surface = gz.Surface(width=size, height=size)
    if halo > 0:
        draw_star(
            surface, [surface.height / 2, surface.height / 2], points, 0, BLACK if inverted else WHITE
        ).draw(surface)
    draw_star(
        surface, [surface.height / 2, surface.height / 2], points, halo, WHITE if inverted else BLACK
    ).draw(surface)
    # Convert to image.
    star72 = surface.get_npimage(transparent=True)
    star72 = (
        Image.fromarray(star72)
        .convert(mode="RGBA")
        .resize((size, size), resample=Image.BICUBIC)
    )
    # Open icon and overlay onto the star.
    icon = (
        Image.open(icon_art)
        .convert(mode="RGBA")
        .resize((size, size), resample=Image.BICUBIC)
    )
    if inverted:
        return Image.alpha_composite(star72, icon)
    else:
        # I know this looks weird; but artwork is black: designed to inverted for a standard black star.
        return Image.alpha_composite(star72, _invert_rgba(icon))
