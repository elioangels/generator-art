const fs = require("fs")
const { createCanvas, loadImage } = require("canvas")

const points = 72
const width = 8000
const height = 2400

const radius = height / 2
const centerLine = width / 2
const canvas = createCanvas(width, height)
const context = canvas.getContext("2d")

function drawStar(cx, cy, spikes, color, outerRadius) {
    var rot = (Math.PI / 2) * 3
    var innerRadius = Math.floor(outerRadius * 0.93) //94
    var x = cx
    var y = cy
    var step = Math.PI / spikes

    context.beginPath()
    context.moveTo(cx, cy - outerRadius)
    for (i = 0; i < spikes; i++) {
        x = cx + Math.cos(rot) * outerRadius
        y = cy + Math.sin(rot) * outerRadius
        context.lineTo(x, y)
        rot += step

        x = cx + Math.cos(rot) * innerRadius
        y = cy + Math.sin(rot) * innerRadius
        context.lineTo(x, y)
        rot += step
    }
    context.lineTo(cx, cy - outerRadius)
    context.closePath()
    // context.lineWidth=5;
    // context.strokeStyle='blue';
    // context.stroke();
    context.fillStyle = color
    context.fill()
}

function drawLogo(cx, cy, text, color, dir) {
    const { height, width } = context.measureText(text)
    context.font = `bold ${Math.floor(cy * 0.6)}pt Montserrat`
    context.textAlign = dir || "center"
    context.textBaseline = "middle"
    context.fillStyle = color
    context.fillText(text, cx, cy)
}

drawLogo(centerLine + radius * 0.84, radius, "Way", "#000000", "left")
drawLogo(centerLine - radius * 0.86, radius * 0.6, "the", "#000000", "right")
drawStar(centerLine, radius, 72, "white", radius)
drawStar(centerLine, radius, 72, "black", radius * 0.93)
drawLogo(centerLine, radius, "elio", "#ffffff")

// loadImage('./logo.png').then(image => {
//   context.drawImage(image, 340, 515, 70, 70)
//   const buffer = canvas.toBuffer('image/png')
//   fs.writeFileSync('./test.png', buffer)
// })

fs.writeFileSync("./test.png", canvas.toBuffer("image/png"))
