#!/usr/bin/env python3
import os
import setuptools
from distutils.core import setup


def read_file_into_string(filename):
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ""


def get_readme():
    if os.path.exists("README.md"):
        return read_file_into_string("README.md")
    return ""


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths


setup(
    author="Tim Bushell",
    author_email="theElioWay@gmail.com",
    description="python App **the elioWay**.",
    entry_points={"console_scripts": ["elio-art=elio_art:main"]},
    include_package_data=True,
    install_requires=["argparse", "gizeh", "Pillow"],
    name="elio-art",
    packages=["art"],
    package_data={"art": [], "fonts": ["*.ttf"], "artwork": ["**"]},
    url="https://elioway.gitlab.io/elioangels/generator-art",
    version="0.0.1",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "Framework :: Django :: 2.2",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development :: Libraries",
    ],
    license="MIT",
    long_description=get_readme(),
    long_description_content_type="text/markdown",
)
