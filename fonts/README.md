# fonts

Add any fonts you want to use here. By default **generator-art** expects:

-   generator-art/fonts/Montserrat-Bold.ttf
-   generator-art/fonts/Montserrat-Regular.ttf
