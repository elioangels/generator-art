<aside>
  <dl>
  <dd>whereof here needs no account;</dd>
  <dd>But rather to tell how, if Art could tell</dd>
</dl>
</aside>

**generator-art** is a resource bank for **elioWay** project artwork, icons and other media.

**the elioWay** is to make branding apps easy, consistent and bold. All the artwork is created in Google Drawings, but any vector drawing program can be used following the guidelines in this documentation.

This repository contains all the base artwork required; while each individual repo links to any public domain images in the credits.
