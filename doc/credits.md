# generator-art Credits

## Core! thanks

-   <https://docs.google.com/drawings>
-   <https://cloudconvert.com>
-   <http://colormind.io>
-   <https://commons.wikimedia.org/>
-   <https://unsplash.com>
-   <https://pixabay.com>
-   <https://www.janvas.com/XOS_6.2/janvas_website_6.2/index.php?language=en&idPage=182>
-   <https://mavo.io/demos/svgpath/>

## Artwork

-   [wikimedia:Roadworks_sign](https://commons.wikimedia.org/wiki/File:Roadworks_sign.JPG)

## Worth looking at

-   <https://www.smiffysplace.com/shapes.html>
-   <https://github.com/lovell/sharp>
