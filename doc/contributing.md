# Contributing

GIT is a fine way to install **art**.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/generator-art.git
cd generator-art
```

## Put in $path

```shell
cd ~/
sudo -H pip3 install -e ~/repo/elioway/elioangels/generator-art
```

### TODOS

1. TODOS
