# Quickstart art

-   [generator-art Prerequisites](/elioangels/generator-art/prerequisites.html)
-   [Installing generator-art](/elioangels/generator-art/installing.html)

## Overview

-   Prepare all **elioWay** repos with the following artwork:

```
elio_app_root
│   apple-touch-icon.png
│   elio-appname-logo.png
│   favicon.ico
│   favicon.ico
|   splash.jpg
│   star.png
```

```
python elio-generator-art.py /
```

Or install and use on the command line.

## Guide

![](/eliosin/icon/star72pt/elio72pt.png)

72pt elio star with black background and a white halo.

![](/eliosin/icon/star72pt/elio24pt.png)

24pt elio star with black background and a white halo. Useful for smaller icons.

![](/eliosin/icon/star72pt/elio12pt.png)

12pt elio star with black background. Useful for smaller favicons.

## Font

Use the [Monserrat Google Font](https://fonts.google.com/specimen/Montserrat)

![](/elio-generator-art-logo.png)

## `elio-appname-logo.png`

Height 180px and width varying with app name length. Full height elio72pt with the word "elio" in the middle font size 48pt. The logo should be followed by the app name in the same font and size; lower case unless for an app group. The elio72pt star should slightly overlap the app name, like it is cutting into it.

![](/eliosin/icon/star72pt/apple-touch-icon.png)

## `apple-touch-icon.png`

180px X 180px png file. Use a elio72pt with a white image centered, representing the app.

![](/eliosin/icon/star72pt/star.png)

## `star.png`

558px X 558px version of `apple-touch-icon.png`. Should be nearly identical, with minor extra details allowed.

![](/eliosin/icon/star72pt/favicon.ico)

## `favicon.ico`, `favicon.ico`

Simplified 32px X 32px versions of `apple-touch-icon.png`. Nearly always changed to accomodate the small size. Use a elio24pt

![](/eliosin/icon/star72pt/splash.jpg)

## `splash.jpg`

Width 558px & Height 270px. An app "Easter egg" with the elio-appname logo in the lower corner; a catchy, public domain image with a theme related to the app; and a joke - if possible.
